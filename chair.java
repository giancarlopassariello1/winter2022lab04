public class chair {
 private String colour;
 private String material;
 private int height;
 
 public void sit(int height) {
  if (height < 0) {
    System.out.println("That's a non existant chair");
  } else {
    System.out.println("nice chair");
  }
 }
 
 public chair (String newColour, String newMaterial, int newHeight) {
	 this.colour = newColour;
	 this.material = newMaterial;
	 this.height = newHeight;
 }

 public String getColour () {
	 return this.colour;
	 
 }
 
 public String getMaterial () {
	 return this.material;
 }
 
 public int getHeight () {
	 return this.height;
 }
 
 public void setColour (String newColour) {
	 this.colour = newColour;
 }
 
 public void setHeight (int newHeight) {
	 this.height = newHeight;
 }
 
}
